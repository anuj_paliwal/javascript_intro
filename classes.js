function Student(fname, lname) {
  this.fname = fname;
  this.lname = lname;
  this.courses = [];
}

function Course(name, dept, credits) {
  this.name = name;
  this.dept = dept;
  this.credits = credits;
  this.students = [];
}

Student.prototype.name = function () {
  return fname + " " + lname;
}

Student.prototype.enroll = function(course) {
  if (this.courses.indexOf(course) === -1) {
    this.courses.push(course);
    course.students.push(this);
  }
};

Student.prototype.courses = function () {
  return this.courses;
}

Student.prototype.course_load = function () {
  var courseload = {};
  for (var i = 0; i < this.courses.length; i++) {
    var course = this.courses[i];
    if (typeof courseload[course.dept] === "undefined") {
      console.log(course.credits);
      courseload[course.dept] = course.credits;
    }
    else {
      courseload[course.dept] += course.credits;
    }
  }

  return courseload;
};

Course.prototype.add_student = function(student) {
  student.enroll(this);
}

var s1 = new Student('James', 'Baldwin');
var s2 = new Student('Richard', 'Wright');
var s3 = new Student('Ralph', 'Ellison');

var calc1 = new Course('Calculus 1', 'Math', 4);
var calc2 = new Course('Calculus 2', 'Math', 4);
var history1 = new Course('History 1', 'BS', 4);

s1.enroll(calc1);
s1.enroll(calc2);
s2.enroll(calc1);

console.log(s1.course_load());

















