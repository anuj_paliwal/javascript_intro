Array.prototype.my_uniq = function () {
  var unique = [];
  for (var i = 0; i < this.length; i++) {
    if (unique.indexOf(this[i]) === -1) {
      unique.push(this[i]);
    }
  }

  return unique
}

Array.prototype.twoSum = function () {
  var indices = [];
  for (var i = 0; i < this.length - 1; i++) {
    for (var j = i + 1; j < this.length; j++) {
      if (this[i] + this[j] === 0) {
        indices.push([i, j]);
      }
    }
  }

  return indices
}

Array.prototype.transpose = function () {
  var transposed = [];
  for (var i = 0; i < this.length; i++) {
    transposed.push([]);
  }

  for (var i = 0; i < this.length; i++) {
    for (var j = 0; j < this[i].length; j++) {
      transposed[j].push(this[i][j]);
    }
  }

  return transposed;
}

Array.prototype.each = function(clbk) {
  for (var i = 0; i < this.length; i++) {
    clbk(this[i]);
  }
}

Array.prototype.map = function(clbk) {
  var mapped = [];
  this.each(function(el) {
    mapped.push(clbk(el));
  });

  return mapped;
}

Array.prototype.myInject = function(clbk) {
  var accum = this[0];
  this.slice(1, this.length).each(function(el) {
    accum = clbk(accum, el);
  });

  return accum;
}

Array.prototype.each_with_index = function(clbk) {
  for (var i = 0; i < this.length; i++) {
    clbk(this[i], i);
  }
}

Array.prototype.bubbleSort = function () {
  var sorted = false;
  while (sorted === false) {
    sorted = true;
    for (var i = 0; i < this.length - 1; i++) {
      if (this[i] > this[i + 1]) {
        sorted = false;
        current = this[i];
        next = this[i + 1];
        this[i + 1] = current;
        this[i] = next;
      }
    }
  }

  return this
}

var substrings = function(str) {
  var subs = [];
  for (var i = 1; i <= str.length; i++) {
    for (var j = 0; j < str.length; j++) {
      var substring = str.slice(j, j + i);
      if (subs.indexOf(substring) === -1) {
        subs.push(substring);
      }
    }
  }

  return subs
}

var range = function(n1, n2) {
  if (n1 === n2) {
    return [n2];
  }
  else {
    return [n1].concat(range(n1 + 1, n2));
  }
}

var fibo = function(n) {
  if (n === 0) {
    return [0];
  }
  else if (n === 1) {
    return [0, 1];
  }
  else {
    var previous = fibo(n - 1);
    lst_num = previous[previous.length - 1];
    bef_lst_num = previous[previous.length - 2];
    return previous.concat([lst_num + bef_lst_num])
  }
}

var bin_srch = function(nums, target) {
  middle = Math.floor(nums.length/2);
  if (nums[middle] === target) {
    return middle;
  }
  else if (target > nums[middle]) {
    right = nums.slice(middle + 1, nums.length);
    return middle + 1 + bin_srch(right, target);
  }
  else {
    left = nums.slice(0, middle);
    return bin_srch(left, target);
  }
}

var make_change = function(amount, coins) {

  if (amount === 0) {
    return [];
  }
  else {
    var sorted_coins = coins.sort().reverse();
    var best_change = [];
    for (var i = 0; i < sorted_coins.length; i++) {
      if (amount >= sorted_coins[i]) {
        var coin = sorted_coins[i];
        var remaining = amount - coin;
        var best_remaining = make_change(remaining, sorted_coins.slice(i, sorted_coins.length));

        var current_change = [coin].concat(best_remaining);
        if (best_change.length === 0 || current_change.length < best_change.length) {
          best_change = current_change;
        }
      }
    }
  }

  return best_change
}

var subsets = function(arr) {
  if (arr.length === 0) {
    return [[]];
  }
  else {
    var last_el = arr[arr.length - 1];
    var previous_sets = subsets(arr.slice(0, arr.length - 1));
    var new_sets = previous_sets.map(function(set) {
      return set.concat(last_el);
    });

    return previous_sets.concat(new_sets);
  }
}

var merge = function(arr1, arr2) {
  var merged = [];
  while (arr1.length !== 0 && arr2.length !== 0) {
    if (arr1[0] <= arr2[0]) {
      merged.push(arr1.shift());
    }
    else {
      merged.push(arr2.shift());
    }
  }

  arr1.length === 0 ? merged = merged.concat(arr2) : merged = merged.concat(arr1)

  return merged;
}

var mergeSort = function(arr) {
  if (arr.length === 1 || arr.length === 0) {
    return arr;
  }
  else {
    var middle = Math.floor(arr.length/2);
    var left = mergeSort(arr.slice(0, middle));
    var right = mergeSort(arr.slice(middle, arr.length));

    return merge(left, right);
  }
}


















