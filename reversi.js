function Board() {
  this.board = this.new_board();
};

function Piece(color) {
  this.color = color;
};

function Game() {
  this.board = new Board()
}

Board.prototype.new_board = function () {
  var starting = [];
  for (var i = 0; i < 8; i++) {
    starting.push([]);
    for (var j = 0; j < 8; j++) {
      if (( i === 3 || i === 4 ) && (i === j)) {
        starting[i].push(new Piece('B'));
      }
      else if ((i === 3 || i === 4) && (j === 3 || j === 4) && (i !== j)) {
        starting[i].push(new Piece('W'));
      }
      else {
        starting[i].push(null);
      }
    }
  }

  return starting
}

Game.prototype.place_piece = function (position, color) {
  //make method that returns valid moves for color
}

var b = new Board();
b.new_board();
console.log(b.board);


