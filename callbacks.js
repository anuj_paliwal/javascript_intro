function times(num, fun) {
  for (var i = 0; i < num; i++) {
    fun(); // call is made "function-style"
  }
}

var cat = {
  age: 5,

  age_one_year: function () {
    this.age += 1;
  }
};

console.log(cat.age);
//cat.age_one_year(); // works

Function.prototype.my_bind = function (obj) {
  obj.this();
}
times(10, cat.age_one_year.my_bind(cat)); // does not work!

console.log(cat.age);